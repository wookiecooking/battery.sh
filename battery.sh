#!/bin/bash
battery=`acpi`
percentage=`acpi | grep -E -o '[0-9]+%' | tail -n1`
batterytime=`acpi -b | grep "Battery 0" | awk -F "," '{print substr($1,1,1) $3}' | sed 's/B //g' | sed 's/ remaining//g' | sed 's/ until charged//g'`

if [[ $battery == *"Discharging"* ]]; then
  echo "BAT: $percentage $batterytime"
  echo $percentage
  echo "#OOC43E"
else
  echo "BAT: $percentage $batterytime"
  echo $percentage
  #echo "#00C43E"
fi

